
## Requirements 

Requires Java 8. To compile the project run 

```bash
./gradlew clean assemble
```

To run the project, run

```bash
java -jar build/libs/builder-0.0.1-SNAPSHOT.jar
```
