package com.burger.builder

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BuilderApplication

fun main(args: Array<String>) {
    InMemoryJsonStore.newDocumentStore("orders") { ArrayList() }

    InMemoryJsonStore.newDocumentStore("ingredients") {
        arrayListOf(mutableMapOf(
                "id" to "ingredients",
                "bacon" to 0,
                "cheese" to 0,
                "meat" to 0,
                "salad" to 0
        ))
    }
    runApplication<BuilderApplication>(*args)
}
