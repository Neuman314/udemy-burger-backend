package com.burger.builder

import java.util.*
import java.util.concurrent.ConcurrentHashMap

object InMemoryJsonStore {

    private val jsonMap: MutableMap<String, MutableList<MutableMap<String, Any>>> = ConcurrentHashMap()

    fun newDocumentStore(document: String, initList: () -> MutableList<MutableMap<String, Any>>) {
        jsonMap[document] = initList()
    }

    fun getAll(document: String): List<Map<String, Any>> = jsonMap[document]!!

    fun getOneBy(document: String, matchCond: (map: Map<String, Any>) -> Boolean): Map<String, Any>? =
            jsonMap[document]!!.let { documents ->
                documents.firstOrNull { matchCond(it) }
            }

    fun add(document: String, obj: MutableMap<String, Any>): MutableMap<String, Any> = obj.apply {
        this["id"] = UUID.randomUUID().toString()
        jsonMap[document]!!.add(obj)
    }

    fun remove(document: String, id: String) {
        val documents = jsonMap[document]
        val docToRemove = documents?.firstOrNull { it["id"] == id }
        documents?.remove(docToRemove)
    }
}