package com.burger.builder.controller

import com.burger.builder.InMemoryJsonStore
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class OrdersController {

    @GetMapping("/")
    fun helloWorld() = ResponseEntity.ok("Hello World!")

    @GetMapping("/orders")
    fun getOrders() = ResponseEntity.ok(InMemoryJsonStore.getAll("orders"))

    @PostMapping("/orders")
    fun addOrder(@RequestBody obj: Map<String, Any>) =
            ResponseEntity.ok(InMemoryJsonStore.add("orders", obj.toMutableMap()))

    @GetMapping("/orders/{id}")
    fun getById(@PathVariable id: String) = InMemoryJsonStore.getOneBy("orders") { it["id"] == id }
            .let {
                when (it != null) {
                    true -> ResponseEntity.ok(it)
                    else -> ResponseEntity.notFound().build()
                }
            }
}