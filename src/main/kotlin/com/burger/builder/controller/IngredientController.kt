package com.burger.builder.controller

import com.burger.builder.InMemoryJsonStore
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class IngredientController {

    @GetMapping("/ingredients")
    fun getIngredients() = ResponseEntity.ok(InMemoryJsonStore.getOneBy("ingredients") {
        it["id"] == "ingredients"
    }!!)
}